package com.bank.bankaccounts.repository;

import com.bank.bankaccounts.entity.BankAccount;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BankAccountRepository extends PagingAndSortingRepository<BankAccount, Long> {

    public List<BankAccount> findByOwnerUid(@Param("ownerUid") String ownerUid);
}
