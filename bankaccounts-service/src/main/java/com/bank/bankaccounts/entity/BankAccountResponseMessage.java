package com.bank.bankaccounts.entity;


public class BankAccountResponseMessage extends ResponseMessage {

    private BankAccount bankAccount;

    public BankAccountResponseMessage(String state, String message, BankAccount bankAccount) {
        super(state, message);
        this.bankAccount = bankAccount;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
}
