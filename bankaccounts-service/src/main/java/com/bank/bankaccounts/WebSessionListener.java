package com.bank.bankaccounts;

import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bank.bankaccounts.entity.BankAccount;
import com.bank.bankaccounts.service.BankAccountService;

public class WebSessionListener implements HttpSessionListener {

    @Autowired
    BankAccountService bankAccountService;

    private static final Logger logger = LoggerFactory.getLogger(WebSessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionCreatedEvent) {
        logger.info("session " + String.valueOf(httpSessionCreatedEvent.getSession().getId()) + " created");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionDestroyedEvent) {
        HttpSession httpSession = httpSessionDestroyedEvent.getSession();
        List<BankAccount> bankAccounts = bankAccountService.findByUid(httpSession.getId());
        for (BankAccount bankAccount : bankAccounts) {
            bankAccountService.delete(bankAccount.getId());
            logger.info("account id: " + String.valueOf(bankAccount.getId()) + " for session is beeing destroyed");
        }
        logger.info("session " + String.valueOf(httpSessionDestroyedEvent.getSession().getId()) + " destroyed");
    }

}