package com.bank.bankaccounts.controller;

import com.bank.bankaccounts.entity.BankAccount;
import com.bank.bankaccounts.entity.BankAccountResponseMessage;
import com.bank.bankaccounts.entity.ResponseMessage;
import com.bank.bankaccounts.service.BankAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;




@RestController
@RequestMapping("/api/v1/bankaccounts")
public class BankAccountsRestController {

    private static final Logger logger = LoggerFactory.getLogger(BankAccountsRestController.class);

    @Autowired
    BankAccountService bankAccountService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<BankAccount> index(HttpServletRequest request, Model model) {
        String sessionId = request.getSession().getId();
        List<BankAccount> bankAccounts = bankAccountService.findByUid(sessionId);
        return bankAccounts;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<ResponseMessage> create(HttpServletRequest request, @RequestBody BankAccount bankAccount, Model model) {
        bankAccount.setOwnerUid(request.getSession().getId());
        bankAccount = bankAccountService.save(bankAccount);
        BankAccountResponseMessage responseMessage = new BankAccountResponseMessage("success", "bankaccount successfully updated", bankAccount);
        return new ResponseEntity<>(responseMessage, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ResponseEntity<ResponseMessage> update(HttpServletRequest request, @PathVariable long id, @RequestBody BankAccount bankAccount) {
        BankAccount origBankAccount = bankAccountService.findOne(bankAccount.getId());
        if (origBankAccount != null) {
            origBankAccount.setBic(bankAccount.getBic());
            origBankAccount.setIban(bankAccount.getIban());
            origBankAccount.setOwnerUid(request.getSession().getId());
            bankAccount = bankAccountService.save(origBankAccount);
            BankAccountResponseMessage responseMessage
                    = new BankAccountResponseMessage("success", "bankaccount successfully updated", bankAccount);
            return new ResponseEntity<>(responseMessage, HttpStatus.OK);
        } else {
            ResponseMessage responseMessage = new ResponseMessage("error", "error updating bankaccount");
            return new ResponseEntity<>(responseMessage, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseMessage> delete(HttpServletRequest request, @PathVariable long id) {
        bankAccountService.delete(id);
        ResponseMessage responseMessage = new ResponseMessage("success", "account successfully deleted");
        return new ResponseEntity<>(responseMessage, HttpStatus.OK);
    }
}
