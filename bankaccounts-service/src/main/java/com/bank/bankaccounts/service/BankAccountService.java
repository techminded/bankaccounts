package com.bank.bankaccounts.service;

import com.bank.bankaccounts.entity.BankAccount;
import com.bank.bankaccounts.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class BankAccountService {

    @Autowired
    BankAccountRepository bankAccountRepository;

    public List<BankAccount> findByUid(String uid) {
        return bankAccountRepository.findByOwnerUid(uid);
    }

    public BankAccount findOne(Long id) {
        return bankAccountRepository.findOne(id);
    }


    public BankAccount save(BankAccount bankAccount) {
        return bankAccountRepository.save(bankAccount);
    }

    public void delete(Long id) {
        bankAccountRepository.delete(id);
    }

    public BankAccount updateFromRequest(HttpServletRequest request, BankAccount bankAccount) {
        bankAccount.setBic(request.getParameter("bic"));
        bankAccount.setIban(request.getParameter("iban"));
        String sessionId = request.getSession().getId();
        bankAccount.setOwnerUid(sessionId);
        return this.save(bankAccount);
    }

}