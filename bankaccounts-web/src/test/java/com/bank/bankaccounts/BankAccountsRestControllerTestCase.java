package com.bank.bankaccounts;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bank.bankaccounts.Application;
import com.bank.bankaccounts.entity.BankAccount;
import com.bank.bankaccounts.util.IntegrationTestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ActiveProfiles("integration")
public class BankAccountsRestControllerTestCase {

	public static final String ENDPOINT_ROOT_PATH = "/api/v1/bankaccounts";

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void testIndex() throws Exception {
		this.mvc.perform(get(ENDPOINT_ROOT_PATH)).andExpect(status().isOk());
	}

	@Test
	public void testCreate() throws Exception {
		BankAccount bankAccount = new BankAccount();
		bankAccount.setBic("12345");
		bankAccount.setIban("1234");
		this.mvc.perform(post(ENDPOINT_ROOT_PATH)
				.contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
				.content(IntegrationTestUtil.convertObjectToJsonBytes(bankAccount)))
			.andExpect(status().isOk())
			.andExpect(jsonPath("bankAccount.id", equalTo(1)))
		;
		this.mvc.perform(post(ENDPOINT_ROOT_PATH)
			.contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
			.content(IntegrationTestUtil.convertObjectToJsonBytes(bankAccount)))
			.andExpect(status().isOk())
			.andExpect(jsonPath("bankAccount.id", equalTo(2)))
		;
	}

	@Test
	public void testUpdate() throws Exception {
		BankAccount bankAccount = new BankAccount();
		bankAccount.setBic("12345");
		bankAccount.setIban("1234");
		bankAccount.setId(3);
		this.mvc.perform(post(ENDPOINT_ROOT_PATH)
				.contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
				.content(IntegrationTestUtil.convertObjectToJsonBytes(bankAccount)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("bankAccount.id", equalTo(3)))
		;
		bankAccount.setIban("1111");
		this.mvc.perform(put(ENDPOINT_ROOT_PATH + "/3")
				.contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
				.content(IntegrationTestUtil.convertObjectToJsonBytes(bankAccount)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("bankAccount.iban", equalTo("1111")))
		;
	}

	@Test
	public void testDelete() throws Exception {
		BankAccount bankAccount = new BankAccount();
		bankAccount.setBic("12345");
		bankAccount.setIban("1234");
		bankAccount.setId(1);
		this.mvc.perform(post(ENDPOINT_ROOT_PATH)
				.contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
				.content(IntegrationTestUtil.convertObjectToJsonBytes(bankAccount)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("bankAccount.id", equalTo(1)));
		this.mvc.perform(delete(ENDPOINT_ROOT_PATH + "/1")
				.contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
		;
		this.mvc.perform(get(ENDPOINT_ROOT_PATH))
				.andExpect(status().isOk())
				.andExpect(content().string("[]"));
	}
}