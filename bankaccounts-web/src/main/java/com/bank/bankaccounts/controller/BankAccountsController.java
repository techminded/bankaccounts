package com.bank.bankaccounts.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class BankAccountsController {
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(Model model) {

        return "classpath:/templates/bankaccounts/index";
    }

}
