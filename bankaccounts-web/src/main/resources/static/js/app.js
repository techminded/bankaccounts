import $ from 'jquery';
import 'bootstrap';

let BANKACCOUNTS_EP_URL = '/api/v1/bankaccounts';

export class AppView {

    constructor(params) {
        this.data = {};
        this.el = $(params.el);
        this.editModal = this.el.find('.edit-modal');
        this.editModal.modal({ show: false });

        this.fields = {
            'iban': { type: 'text', required: true},
            'bic': { type: 'text', required: true},
            'id': { type: 'text', required: false}
        };

        this.grid = this.el.find('table tbody');
        console.log('app view loaded');
    }

    render() {
        this.loadAccounts();
        this.bindActions();
    }

    loadAccounts() {
        $.ajax({
            url: BANKACCOUNTS_EP_URL,
            dataType: 'json',
            success: function(data) {
                this.grid.html('');
                for (var i in data) {
                    this.data[i] = data[i];
                    this.renderRow(i);
                }
            }.bind(this)
        });
    }

    bindActions() {
        this.el.on('click', '.btn-add', function(event) {
            this.onAddClicked(event);
        }.bind(this));

        this.editModal.on('click', '.btn-primary', function(event) {
            this.onSaveClicked(event);
        }.bind(this));

        this.grid.on('click', '.btn-edit', function(event) {
            this.onEditClicked(event);
        }.bind(this));

        this.grid.on('click', '.btn-delete', function(event) {
            this.onDeleteClicked(event);
        }.bind(this));
    }

    onAddClicked(event) {
        this.updateForm({});
        this.editModal.modal('show');
    }

    onEditClicked(event) {
        let num = $(event.target).attr('data-ref');
        let account = this.data[num];

        for (let fieldName of Object.keys(this.fields)) {
            this.editModal.find('#' + fieldName).val(account[fieldName]);
        }
        this.editModal.modal('show');
    }

    onSaveClicked(event) {
        event.preventDefault();
        if (! this.validate()) {
            alert("Fill the form properly, please");
            return false;
        }

        var id = parseInt(this.editModal.find('#id').val()),
            url = BANKACCOUNTS_EP_URL,
            requestType = 'post';

        if (id > 0) {
            url = BANKACCOUNTS_EP_URL + '/' + id;
            requestType = 'put';
        }

        $.ajax({
            type: requestType,
            url: url,
            data: JSON.stringify(this.loadFormData()),
            dataType: 'json',
            contentType: "application/json",
            success: function(data) {
                var id = parseInt(data.id);
                this.updateForm({});
                this.loadAccounts();
                this.editModal.modal('hide');
            }.bind(this)
        })
    }

    onDeleteClicked(event) {
        let num = $(event.target).attr('data-ref'),
            account = this.data[num];

        $.ajax({
            url: BANKACCOUNTS_EP_URL + '/' + account.id,
            type: 'delete',
            dataType: 'json',
            success: function(data) {
                this.loadAccounts();
            }.bind(this),
            error: function() {
                alert('Error deleting account');
            }
        });
    }

    validate() {
        for (let fieldName of Object.keys(this.fields)) {
            var field = this.fields[fieldName],
                value = this.editModal.find('#' + fieldName).val();
            if (field.required) {
                if (value == undefined || value == '') {
                    return false;
                }
            }
            if (field.type == 'number') {
                if (! parseFloat(value)) {
                    return false;
                }
            }
            if (field.type == 'date') {
                if (! this.isValidDate(value)) {
                    return false;
                }
            }
        }
        return true;
    }

    updateForm(data) {
        for (let fieldName of Object.keys(this.fields)) {
            var value = data[fieldName] != undefined ? data[fieldName] : '';
            this.editModal.find('#' + fieldName).val(value);
        }
    }

    loadFormData() {
        var data = {};

        for (let fieldName of Object.keys(this.fields)) {
            data[fieldName] = this.editModal.find('#' + fieldName).val();
        }
        return data;
    }

    renderRow(num) {
        let account = this.data[num];

        this.grid.append(
            `<tr class="row-account-${num}">
                <td class="row-account-id">${account.id}</td>
                <td class="row-account-iban">${account.iban}</td>
                <td class="row-account-secondName">${account.bic}</td>
                <td class="actions">
                    <button type="button" data-ref="${num}" class="btn-edit">Edit</button>
                    <button type="button" data-ref="${num}" class="btn-delete">Delete</button>
                </td>
            </tr>`
        );
    }
}