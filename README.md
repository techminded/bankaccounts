# Bank Accounts #

## prepare ##

You should install javascript artifacts. Nodejs and npm must be present on your pc. 
In addition jspm package manager must be installed, you can do this with the following command:

```
#!bash
cd bankaccounts-web/src/main/resources/static
npm intall jspm
```
then let jspm download and install all deps required
```
#!bash

./node_modules/.bin/jspm install

```

## running ##
```
#!bash
mvn spring-boot:run
```
## running tests ##
```
#!bash
mvn test
```